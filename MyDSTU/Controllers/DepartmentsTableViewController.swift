//
//  DepartmentsTableViewController.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 12.10.2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import UIKit

class DepartmentsTableViewController: UITableViewController {
    
    var departments = [DepartmentItem]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "Background"))
        
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = .gray
        refreshControl?.addTarget(self, action: #selector(loadData), for: .valueChanged)
        
        loadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DepartmentsToItemVC" {
            if let VC = segue.destination as? DepartmentsItemViewController, let sender = sender as? [String] {
                VC.title = sender[0]
                VC.id = sender[1]
            }
        }
    }
    
    @objc fileprivate func loadData() {
        refreshControl?.beginRefreshing()
        NetworkManager.shared.request(url: "getDepartments") { (result) in
            switch result {
            case .failure(let error):
                self.presentAlert(message: "Упс, что-то пошло не так!")
                print(error, error.localizedDescription)
            case .success(let data):
                //self.printJSON(data: data)
                do {
                    let response = try JSONDecoder().decode(GetDepartmentsResponse.self, from: data)
                    self.departments = response.list
                    self.tableView.reloadData()
                } catch {
                    self.presentAlert(message: "Упс, что-то пошло не так!")
                    print(error, error.localizedDescription)
                }
            }
            self.refreshControl?.endRefreshing()
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return departments.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DepartmentCell", for: indexPath) as! DepartmentTableViewCell
        cell.departmentItem = departments[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = departments[indexPath.row]
        performSegue(withIdentifier: "DepartmentsToItemVC", sender: [item.title, item.id])
    }
}

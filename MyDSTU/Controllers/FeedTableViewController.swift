//
//  FeedTableViewController.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 12.10.2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import UIKit

class FeedTableViewController: UITableViewController {
    
    var feedItems = [FeedItem]()
    var currentPage = 0
    var totalPages: Int!
    var isLoading = false

    override func viewDidLoad() {
        super.viewDidLoad()
        //navigationItem.searchController = UISearchController()
        
        tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "Background"))
        
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = .gray
        refreshControl?.addTarget(self, action: #selector(loadData), for: .valueChanged)
        
        loadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NewsToItemVC" {
            if let VC = segue.destination as? FeedItemViewController, let sender = sender as? [String] {
                VC.title = sender[0]
                VC.id = sender[1]
            }
        }
    }
    
    @objc fileprivate func loadData() {
        print("Load data")
        if isLoading { return }
        print("Load started")
        isLoading = true
        refreshControl?.beginRefreshing()
        
        NetworkManager.shared.request(url: "getFeed?page=\(currentPage)") { (result) in
            switch result {
            case .failure(let error):
                self.presentAlert(message: "Упс, что-то пошло не так!")
                print(error.localizedDescription)
            case .success(let data):
                //self.printJSON(data: data)
                do {
                    let response = try JSONDecoder().decode(GetFeedResponse.self, from: data)
                    if self.currentPage == 0 {
                        self.feedItems = response.data
                    } else {
                        self.feedItems += response.data
                    }
                    self.totalPages = response.totalpages
                    self.tableView.reloadData()
                    print("Load completed")
                } catch {
                    self.presentAlert(message: "Упс, что-то пошло не так!")
                    print(error, error.localizedDescription)
                }
            }
            self.refreshControl?.endRefreshing()
            self.isLoading = false
            print("Load ended")
        }
    }

    // MARK: - Table view delegate and data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath) as! FeedTableViewCell
        cell.feedItem = feedItems[indexPath.row]
        cell.titleLabel.text = feedItems[indexPath.row].title
        cell.dateLabel.text = feedItems[indexPath.row].publish
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (UIScreen.main.bounds.width - 30) / CGFloat(feedItems[indexPath.row].preview.width) * CGFloat(feedItems[indexPath.row].preview.height) + 22
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = feedItems[indexPath.row]
        performSegue(withIdentifier: "NewsToItemVC", sender: [item.title, item.id])
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentSize.height < 500 { return }
        if scrollView.contentSize.height - scrollView.contentOffset.y - UIScreen.main.bounds.height < 500 && currentPage + 1 < totalPages {
            currentPage += 1
            loadData()
        }
    }
}

//
//  EditAccountViewController.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 06/10/2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import UIKit

class EditAccountViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var textFields: [UITextField]!
    
    var user: User!

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        for textField in textFields {
            textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder ?? String(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        textFields[0].text = user.surname
        textFields[1].text = user.name
        textFields[2].text = user.group
        textFields[3].text = user.faculty
        textFields[4].text = user.course
    }
    
    @objc
    fileprivate func keyboardWillShow(notification: Notification) {
        if let userInfo = notification.userInfo, let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            scrollView.contentOffset = CGPoint(x: 0, y: frame.height)
        }
    }
    
    @objc
    fileprivate func keyboardWillHide(notification: Notification) {
        scrollView.contentOffset = .zero
    }
    
    @IBAction func okButtonTapped() {
        for textField in textFields {
            textField.resignFirstResponder()
        }
        if textFields[0].text!.isEmpty || textFields[1].text!.isEmpty {
            self.presentAlert(message: "Фамилия и имя обязательные поля")
            return
        }
        
        NetworkManager.shared.request(url: "editUser", method: .post, parameters: ["surname": textFields[0].text!, "name": textFields[1].text!, "group": textFields[2].text!, "faculty": textFields[3].text!, "course": textFields[4].text!], sendCookies: true) { (result) in
            switch result {
            case .failure(let error):
                self.presentAlert(message: "Упс, что-то пошло не так!")
                print(error.localizedDescription)
            case .success(let data):
                //self.printJSON(data: data)
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        if let error = json["error"] as? String {
                            self.presentAlert(message: error)
                        } else {
                            if json["answer"] != nil {
                                let alertVC = UIAlertController(title: nil, message: "Информация успешно изменена", preferredStyle: .alert)
                                let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                    self.navigationController?.popViewController(animated: true)
                                })
                                alertVC.addAction(alertAction)
                                self.present(alertVC, animated: true, completion: nil)
                            }
                        }
                    }
                } catch {
                    self.presentAlert(message: "Упс, что-то пошло не так!")
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    @IBAction func viewTapped(_ sender: Any) {
        for textField in textFields {
            textField.resignFirstResponder()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

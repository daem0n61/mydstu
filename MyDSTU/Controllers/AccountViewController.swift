//
//  AccountViewController.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 29/06/2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {
    
    @IBOutlet weak var accountBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var editBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet var labels: [UILabel]!
    
    var user: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        removeUsersData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        avatarImage.layer.cornerRadius = avatarImage.frame.width / 2
        loadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "AccountToLoginVC":
            if let navController = segue.destination as? UINavigationController, let VC = navController.viewControllers[0] as? LoginViewController {
                VC.delegate = self
            }
        case "AccountToEditVC":
            if let VC = segue.destination as? EditAccountViewController {
                VC.user = user
            }
        default:
            break
        }
    }
    
    func loadData() {
        if HTTPCookieStorage.shared.cookies!.isEmpty {
            accountBarButtonItem.image = #imageLiteral(resourceName: "LogIn")
            performSegue(withIdentifier: "AccountToLoginVC", sender: nil)
        } else {
            NetworkManager.shared.request(url: "getUser", sendCookies: true) { (result) in
                switch result {
                case .failure(let error):
                    self.presentAlert(message: "Упс, что-то пошло не так!")
                    print(error.localizedDescription)
                case .success(let data):
                    //self.printJSON(data: data)
                    do {
                        self.user = try JSONDecoder().decode(User.self, from: data)
                        self.editBarButtonItem.isEnabled = true
                        self.accountBarButtonItem.image = #imageLiteral(resourceName: "LogOut")
                        self.labels[0].text = self.user.surname + " " + self.user.name
                        self.labels[1].text = self.user.email
                        self.labels[2].text = self.user.group
                        self.labels[3].text = self.user.faculty
                        self.labels[4].text = self.user.course
                    } catch {
                        self.presentAlert(message: "Упс, что-то пошло не так!")
                        print(error, error.localizedDescription)
                    }
                }
            }
        }
    }
    
    fileprivate func removeUsersData() {
        editBarButtonItem.isEnabled = false
        accountBarButtonItem.image = #imageLiteral(resourceName: "LogIn")
        for label in labels {
            label.text = ""
        }
        if HTTPCookieStorage.shared.cookies!.isEmpty {
            labels[0].text = "Для просмотра личных данных или оценки новостей зарегистрируйтесь или войдите в аккаунт"
        }
    }
    
    @IBAction func accountBarButtonItemPressed(_ sender: Any) {
        if HTTPCookieStorage.shared.cookies!.isEmpty {
            performSegue(withIdentifier: "AccountToLoginVC", sender: nil)
        } else {
            NetworkManager.shared.request(url: "logOut", method: .post, getCookies: true, sendCookies: true) { (result) in
                switch result {
                case .failure(let error):
                    self.presentAlert(message: "Упс, что-то пошло не так!")
                    print(error, error.localizedDescription)
                case .success(let data):
                    self.printJSON(data: data)
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                            if let answer = json["answer"] as? String, answer == "success" {
                                print("Logged out")
                            } else {
                                self.presentAlert(message: "Упс, что-то пошло не так!")
                            }
                        }
                    } catch {
                        self.presentAlert(message: "Упс, что-то пошло не так!")
                        print(error.localizedDescription)
                    }
                }
            }
            HTTPCookieStorage.shared.removeCookies(since: Date(timeIntervalSince1970: 0))
            removeUsersData()
        }
    }
}

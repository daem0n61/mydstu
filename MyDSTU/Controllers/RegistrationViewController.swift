//
//  RegistrationViewController.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 28/05/2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var textFields: [UITextField]!
    
    var delegate: AccountViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        for textField in textFields {
            textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder ?? String(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    @objc
    fileprivate func keyboardWillShow(notification: Notification) {
        if let userInfo = notification.userInfo, let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            scrollView.contentOffset = CGPoint(x: 0, y: frame.height)
        }
    }
    
    @objc
    fileprivate func keyboardWillHide(notification: Notification) {
        scrollView.contentOffset = .zero
    }
    
    @IBAction func registrationButtonTapped() {
        for textField in textFields {
            textField.resignFirstResponder()
        }
        for textField in textFields {
            if textField.text!.isEmpty {
                self.presentAlert(message: "Заполните пустые поля")
                return
            }
        }
        if textFields[3].text! != textFields[4].text! {
            self.presentAlert(message: "Пароли должны совпадать")
            return
        }
        NetworkManager.shared.request(url: "registration", method: .post, parameters: ["name": textFields[0].text!, "surname": textFields[1].text!, "email": textFields[2].text!, "password": textFields[3].text!]) { (result) in
            switch result {
            case .failure(let error):
                self.presentAlert(message: "Упс, что-то пошло не так!")
                print(error.localizedDescription)
            case .success(let data):
                //self.printJSON(data: data)
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        if let error = json["error"] as? String {
                            self.presentAlert(message: error)
                        } else {
                            if let answer = json["answer"] as? String {
                                let alertVC = UIAlertController(title: nil, message: answer, preferredStyle: .alert)
                                let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                    NetworkManager.shared.request(url: "login", method: .post, parameters: ["login": self.textFields[2].text!, "password": self.textFields[3].text!], getCookies: true) { (result) in
                                        switch result {
                                        case .failure(let error):
                                            self.presentAlert(message: "Упс, что-то пошло не так!")
                                            print(error.localizedDescription)
                                        case .success(let data):
                                            do {
                                                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                                                    if let error = json["error"] as? String {
                                                        self.delegate.presentAlert(message: error)
                                                    } else {
                                                        self.delegate.loadData()
                                                        //self.navigationController?.dismiss(animated: true, completion: nil)
                                                    }
                                                }
                                            } catch {
                                                self.presentAlert(message: "Упс, что-то пошло не так!")
                                                print(error.localizedDescription)
                                            }
                                        }
                                    }
                                    //self.delegate.loadData()
                                    self.navigationController?.dismiss(animated: true, completion: nil)
                                })
                                alertVC.addAction(alertAction)
                                self.present(alertVC, animated: true, completion: nil)
                            }
                        }
                    }
                } catch {
                    self.presentAlert(message: "Упс, что-то пошло не так!")
                    print(error.localizedDescription)
                }
            }
            
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

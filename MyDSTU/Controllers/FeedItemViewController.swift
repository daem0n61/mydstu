//
//  FeedItemViewController.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 29/06/2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import UIKit
import WebKit

class FeedItemViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet var ratingButtons: [UIButton]!
    @IBOutlet weak var ratingLabel: UILabel!
    
    var id, rating: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.isOpaque = false
        webView.backgroundColor = .clear
        webView.scrollView.backgroundColor = .clear
        
        for index in 0...4 {
            ratingButtons[index].tag = index
        }
        
        NetworkManager.shared.request(url: "getFeedItem?id=\(id!)") { (result) in
            switch result {
            case .failure(let error):
                self.presentAlert(message: "Упс, что-то пошло не так!")
                print(error.localizedDescription)
            case .success(let data):
                //self.printJSON(data: data)
                do {
                    let news = try JSONDecoder().decode(FeedItemDetail.self, from: data)
                    self.webView.loadHTMLString(news.text, baseURL: nil)
                    self.rating = news.rating
                    self.ratingLabel.text = self.rating
                    self.setRatingStars()
                } catch {
                    self.presentAlert(message: "Упс, что-то пошло не так!")
                    print(error, error.localizedDescription)
                }
            }
        }
    }
    
    fileprivate func setRatingStars() {
        if rating == "0" {
            for button in ratingButtons {
                button.setImage(#imageLiteral(resourceName: "StarUnranked"), for: .normal)
            }
        } else {
            if let rating = Float(rating) {
                let intRating = Int(rating)
                for index in 0...intRating - 1 {
                    ratingButtons[index].setImage(#imageLiteral(resourceName: "StarFilled"), for: .normal)
                }
                let fractRating = rating - Float(intRating)
                if fractRating < 0.25 {
                    ratingButtons[intRating].setImage(#imageLiteral(resourceName: "StarEmpty"), for: .normal)
                } else {
                    if fractRating < 0.75 {
                         ratingButtons[intRating].setImage(#imageLiteral(resourceName: "StarHalf"), for: .normal)
                    } else {
                         ratingButtons[intRating].setImage(#imageLiteral(resourceName: "StaFilled"), for: .normal)
                    }
                }
            } else {
                self.presentAlert(message: "Не удалось получить рейтинг")
            }
        }
    }
    
    @IBAction func ratingButtonPressed(_ sender: UIButton) {
        if HTTPCookieStorage.shared.cookies!.isEmpty {
            self.presentAlert(message: "Чтобы оценить новость нужно войти в личный кабинет или зарегистрироваться")
        } else {
            if rating == "0" {
                NetworkManager.shared.request(url: "setFeedRating", method: .post, parameters: ["id": id!, "rating": sender.tag + 1], sendCookies: true) { (result) in
                    switch result {
                    case .failure(let error):
                        self.presentAlert(message: "Упс, что-то пошло не так!")
                        print(error.localizedDescription)
                    case .success(let data):
                        //self.printJSON(data: data)
                        do {
                            let response = try JSONDecoder().decode(SetFeedRatingResponse.self, from: data)
                            if response.answer == "error" {
                                self.presentAlert(message: "Упс, что-то пошло не так!")
                            } else {
                                self.rating = response.answer
                                self.ratingLabel.text = self.rating
                                self.setRatingStars()
                            }
                        } catch {
                            self.presentAlert(message: "Упс, что-то пошло не так!")
                            print(error, error.localizedDescription)
                        }
                    }
                }
            } else {
                self.presentAlert(message: "Вы уже оценили эту запись")
            }
        }
    }
}

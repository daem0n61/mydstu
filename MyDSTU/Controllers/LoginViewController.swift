//
//  LoginViewController.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 28/05/2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet var textFields: [UITextField]!
    
    var delegate: AccountViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        for textField in textFields {
            textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder ?? String(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LoginToRegVC", let VC = segue.destination as? RegistrationViewController {
            VC.delegate = delegate
        }
    }
    
    @objc fileprivate func keyboardWillShow(notification: Notification) {
        if let userInfo = notification.userInfo, let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.5) {
                self.logoImageView.alpha = 0
            }
            scrollView.contentOffset = CGPoint(x: 0, y: frame.height)
        }
    }
    
    @objc fileprivate func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.5) {
            self.logoImageView.alpha = 1
        }
        scrollView.contentOffset = .zero
    }
    
    @IBAction func loginButtonTapped() {
        for textField in textFields {
            textField.resignFirstResponder()
        }
        /*for textField in textFields {
            if let text = textField.text, text.isEmpty {
                self.presentAlert(message: "Заполните пустые поля")
                return
            }
        }*/
        NetworkManager.shared.request(url: "login", method: .post, parameters: ["login": "iOStest" /*textFields[0].text!*/, "password": "test" /*textFields[1].text!*/], getCookies: true) { (result) in
            switch result {
            case .failure(let error):
                self.presentAlert(message: "Упс, что-то пошло не так!")
                print(error.localizedDescription)
            case .success(let data):
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        if let error = json["error"] as? String {
                            self.presentAlert(message: error)
                        } else {
                            print("logged in")
                            self.delegate.loadData()
                            self.navigationController?.dismiss(animated: true, completion: nil)
                        }
                    }
                } catch {
                    self.presentAlert(message: "Упс, что-то пошло не так!")
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

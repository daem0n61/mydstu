//
//  DepartmentsItemViewController.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 01/07/2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import UIKit
import WebKit
import MapKit

class DepartmentsItemViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var mapBarButtonItem: UIBarButtonItem!
    
    var id: String!
    var department: DepartmentItemDetail!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapBarButtonItem.isEnabled = false
        
        webView.isOpaque = false
        webView.backgroundColor = .clear
        webView.scrollView.backgroundColor = .clear
        
        NetworkManager.shared.request(url: "getDepartmentsItem?id=\(id!)") { (result) in
            switch result {
            case .failure(let error):
                self.presentAlert(message: "Упс, что-то пошло не так!")
                print(error, error.localizedDescription)
            case .success(let data):
                //self.printJSON(data: data)
                do {
                    self.department = try JSONDecoder().decode(DepartmentItemDetail.self, from: data)
                    self.mapBarButtonItem.isEnabled = true
                    self.webView.loadHTMLString(self.department.text, baseURL: nil)
                } catch {
                    self.presentAlert(message: "Упс, что-то пошло не так!")
                    print(error, error.localizedDescription)
                }
            }
        }
    }
    
    @IBAction func mapBarButtonItemTapped(_ sender: Any) {
        if let latitude = Double(department.latitude), let longitude = Double(department.longitude) {
            let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
            let region = MKCoordinateRegion(center: coordinate, span: MKCoordinateSpan(latitudeDelta: 0.0001, longitudeDelta: 0.0002))
            let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: region.center),
                           MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: region.span)]
            mapItem.name = department.title
            mapItem.openInMaps(launchOptions: options)
        }
    }
}

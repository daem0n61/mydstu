//
//  AnimatedImageViewController.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 12.10.2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import UIKit
import SDWebImage

class AnimatedImageViewController: UIViewController {
    
    @IBOutlet weak var animatedImageView: SDAnimatedImageView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let animatedImage = SDAnimatedImage(named: "Logo.gif") {
            animatedImageView.image = animatedImage
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.8) {
            self.performSegue(withIdentifier: "AnimatedToTabBarVC", sender: nil)
            //self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func screenTapped(_ sender: Any) {
        performSegue(withIdentifier: "AnimatedToTabBarVC", sender: nil)
        //self.dismiss(animated: false, completion: nil)
    }
}

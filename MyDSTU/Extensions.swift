//
//  Extensions.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 28/05/2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func printJSON(data: Data) {
        print(data)
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                print(json)
            } else {
                print("Wrong JSON")
            }
        } catch {
            print("Not valid data")
        }
    }
    
    func presentAlert(message: String) {
        let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertVC.addAction(alertAction)
        present(alertVC, animated: true, completion: nil)
    }
}

//
//  FeedTableViewCell.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 29/05/2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import UIKit
import SDWebImage

class FeedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var feedItem: FeedItem! {
        didSet {
            titleLabel.text = feedItem.title
            dateLabel.text = feedItem.publish
            
            logoImageView.sd_setImage(with: URL(string: NetworkManager.shared.baseUrl + "getPic?picname=" + feedItem.ava), completed: nil)
            mainImageView.sd_setImage(with: URL(string: NetworkManager.shared.baseUrl + "getPic?picname=" + feedItem.preview.url), completed: nil)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        mainView.layer.cornerRadius = 40
    }
    
    override func prepareForReuse() {
        mainImageView.image = nil
        titleLabel.text = nil
        dateLabel.text = nil
    }
}

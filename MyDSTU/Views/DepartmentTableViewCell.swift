//
//  DepartmentTableViewCell.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 01/07/2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import UIKit

class DepartmentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var departmentItem: DepartmentItem! {
        didSet {
            titleLabel.text = departmentItem.title
            logoImageView.sd_setImage(with: URL(string: NetworkManager.shared.baseUrl + "getPic?picname=" + departmentItem.logo))
        }
    }
    
    
    override func layoutSubviews() {
        mainView.layer.cornerRadius = 24
    }
    
    override func prepareForReuse() {
        logoImageView.image = nil
        titleLabel.text = nil
    }
}

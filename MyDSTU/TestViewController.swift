//
//  TestViewController.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 12.10.2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import UIKit
import SDWebImage

class TestViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "Background"))
    }
}

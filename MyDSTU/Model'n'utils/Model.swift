//
//  Model.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 28/05/2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import Foundation

class Model {
    
    static let shared = Model()
    
    var feedItems = [FeedItem]()
    var currentPage = 0
    var totalPages: Int!
    var isLoading = false
}

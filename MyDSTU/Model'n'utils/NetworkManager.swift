//
//  NetworkManager.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 08/10/2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import Foundation

enum URLMethod {
    case get, post
}

class NetworkManager {
    
    static let shared = NetworkManager()
    
    private let debug = false, printData = false, printCookies = false
    let baseUrl = "http://46.229.215.224:9010/api/"
    
    fileprivate func printJSON(data: Data) {
        print(data)
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                print(json)
            } else {
                print("Wrong JSON")
            }
        } catch {
            print("Not valid data")
        }
    }
    
    func request(url: String, method: URLMethod = .get, parameters: [String: Any] = [:], getCookies: Bool = false, sendCookies: Bool = false, _ completion: @escaping (Result<Data, Error>) -> Void) {
        guard let url = URL(string: baseUrl + url) else {
            print("URL not handled")
            return
        }
        var session = URLSession(configuration: .ephemeral)
        if getCookies {
            session = URLSession.shared
        }
        var request = URLRequest(url: url)
        if method == .post {
            request.httpMethod = "post"
            request.addValue("application/json", forHTTPHeaderField: "Content-type")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options:[]) else {
                print("Parameters not handled")
                return
            }
            request.httpBody = httpBody
        }
        if sendCookies {
            request.allHTTPHeaderFields = HTTPCookie.requestHeaderFields(with: HTTPCookieStorage.shared.cookies!)
        }
        session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                completion(.failure(error))
            } else {
                /*guard let response = response else {
                    print("Response not handled")
                    return
                }*/
                guard let data = data else {
                    print("Data not handled")
                    return
                }
                if self.debug {
                    print(request.url!)
                    if let body = request.httpBody {
                        self.printJSON(data: body)
                    }
                    print(request.allHTTPHeaderFields ?? "Empty headers")
                    //print(response)
                    if self.printData {
                        self.printJSON(data: data)
                    }
                    self.printCookies ? print(HTTPCookieStorage.shared.cookies!) : print(HTTPCookieStorage.shared.cookies!.isEmpty ? "CookieStorage is empty" : "Cookie exists")
                }
                DispatchQueue.main.async {
                    completion(.success(data))
                }
            }
        }.resume()
    }
}

//
//  Types.swift
//  MoyDGTU
//
//  Created by Дмитрий Жданов on 28/05/2019.
//  Copyright © 2019 t-rex studio. All rights reserved.
//

import Foundation

struct GetFeedResponse: Codable {
    let data: [FeedItem]
    let totalpages: Int
}

struct FeedItem: Codable {
    let id, title, date, publish, type, ava: String
    let preview: Image
}

struct FeedItemDetail: Codable {
    let text, rating: String
}

struct SetFeedRatingResponse: Codable {
    let answer: String
}

struct GetDepartmentsResponse: Codable {
    let list: [DepartmentItem]
    //let timestampt: TimeInterval
}

struct DepartmentItem: Codable {
    let id, logo, title: String
}

struct DepartmentItemDetail: Codable {
    let text, title, latitude, longitude: String
}

struct User: Codable {
    let surname, name, email: String
    let group, faculty, course : String?
}

struct Image: Codable {
    let height, width: Int
    let url: String
}
